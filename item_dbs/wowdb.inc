<?php


/**
 * @file
 *   Provide WOW Lootz hooks for the Wowdb.com (http://www.wowdb.com) WOW database
 */

define('WOWDB_BASE_URL', 'http://www.wowdb.com');
define('WOWDB_SEARCH_NAME', 'search.aspx?search_text=');
define('WOWDB_SEARCH_ID', 'item.aspx?id=');

/**
 * Implementation of lootz_hook_item_db_info()
 */
function lootz_wowdb_item_db_info() {
  return array('wowdb' => t('<a href="!url">WoW DB</a>'));
}

/**
 * Implementation of lootz_hook_filter_description()
 */
function lootz_wowdb_filter_description() {
  return t('Converts [item] tags to links/tooltips of the configured item database (<a href="!url">WoW DB</a> is the default).', array('!url' => WOWDB_BASE_URL));
}

/**
 * Implementation of lootz_hook_filter_tips()
 */
function lootz_wowdb_filter_tips() {
  return t('You can include [item][/item] tags to automatically link the items to <a href="!url">Wow DB</a>. Either use the <em>Item ID</em> ([item]1234[/item]) or the <em>exact name</em> ([item]Earthwarden[/item])', array('!url' => WOWDB_BASE_URL));
}

/**
 * Implementation of lootz_hook_id_lookup()
 */
function lootz_wowdb_id_lookup($name) {
  $url = WOWDB_BASE_URL . '/' . WOWDB_SEARCH_NAME . drupal_urlencode($name);
  $page = drupal_http_request($url);
  if ($page->code == '200') {
    // find id via this redirect tag: <script>self.location="item.aspx?id=29171";</script>
    if (preg_match('/self\.location="'. preg_quote(WOWDB_SEARCH_ID) . '([0-9]+)";/', $page->data, $m)) {
      return check_plain($m[1]);
    }
  }
  return false;
}

/**
 * Implementation of lootz_hook_name_lookup()
 */
function lootz_wowdb_name_lookup($id) {
  // grab search results
  $page = drupal_http_request(WOWDB_BASE_URL . '/' . WOWDB_SEARCH_ID . $id);
  if ($page->code == '200') {

    // search for title tag of the form:
    // <h1 id="ctl00_MainContent_PageHeader" title="World of Warcraft Halberd of Desolation">Halberd of Desolation</h1>
    if (preg_match('/<h1 id="ctl00_MainContent_PageHeader" title=".*">(.*)<\/h1>/i', $page->data, $m)) {
      return check_plain($m[1]);
    }
  }
  return $id;
}

/**
 * Implementation of lootz_hook_link_item()
 */
function lootz_wowdb_link_item($name, $id) {
  return l($name, WOWDB_BASE_URL . '/' . WOWDB_SEARCH_ID . $id, array('title' => check_plain($name), 'id' => 'item_' .$id));
}

/**
 * Implementation of lootz_hook_init()
 */
function lootz_wowdb_init() {
  // include the external js
  drupal_set_html_head('<script src="http://www.wowdb.com/js/extooltips.js?8" type="text/javascript" ></script>'."\n");
}
