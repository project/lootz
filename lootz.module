<?php


/**
 * @file
 *   Allow the use of [item] filters. The item databases are 
 *   configured in the item_dbs directory. See item_dbs/wowdb.inc
 *   for an example.
 *
 * @todo
 *   Add more (eg, wowhead, thottbot, etc)
 */

/**
 * Implementation of hook_filter()
 */
function lootz_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(0 => t('MMORPG item filter'));

    case 'description':
      return _lootz_invoke_hook('filter_description');

    case 'prepare' :
      return $text;
    case 'process':
      return _lootz_filter_process($text, $format);
    case 'settings' :
      return _lootz_filter_settings();
  }
}

/**
 * Implementation of hook_filter_tips()
 */
function lootz_filter_tips($delta, $format, $long) {
  return _lootz_invoke_hook('filter_tips', $delta, $format, $long);
}

/**
 * Replace [item] tags with corresponding values, if found
 */
function _lootz_filter_process($text, $format) {
  // find all links
  if (preg_match_all('#\[item\]([0-9a-zA-Z ]+)\[/item\]#',$text, $links, PREG_SET_ORDER)) {
    foreach ($links as $match) {
      if ($new_link = _lootz_link_item($match[1])) {
        $text = str_replace($match[0], $new_link, $text);
      }
    }
  }
  return $text;
}

/**
 * Implementation of hook_init()
 */
function lootz_init() {
  _lootz_invoke_hook('init');
}

/**
 * @param id string|int   Item ID or Name
 * @return string         Link to item in configured item db
 */
function _lootz_link_item($id) {
  if (is_numeric($id)) {
    // find the name
    $name = _lootz_name_lookup($id);
  }
  else {
    // find id
    $name = $id;
    $id = _lootz_id_lookup($id);
  }
  return _lootz_invoke_hook('link_item', $name, $id);
}

/**
 * Find an item name based on ID
 * @param id int   Item ID
 * @return string  Item name
 */
function _lootz_name_lookup($id) {
  static $names = array();

  if (empty($names)) {
    $names = _lootz_cache_get('names');
  }

  if (!isset($names[$id])) {
    $names[$id] = _lootz_invoke_hook('name_lookup', $id);
    _lootz_cache_set('names', $names);
  }

  return $names[$id];
}

/**
 * Find an item id based on name
 * @param name string   Item Name
 * @return int          Item ID
 */
function _lootz_id_lookup($name) {
  static $ids = array();
  if (empty($ids)) {
    $ids = _lootz_cache_get('ids');
  }

  if (!isset($ids[$name])) {
    $ids[$name] = _lootz_invoke_hook('id_lookup', $name);
    _lootz_cache_set('ids', $ids);
  }

  return $ids[$name];
}

/**
 * Invoke lootz hooks
 */
function _lootz_invoke_hook() {
  $args = func_get_args();
  $hook = array_shift($args);

  // determine which vendor to use (default is wowdb)
  $vendor = variable_get('lootz_item_database', 'wowdb');
  require_once 'item_dbs/' . $vendor .'.inc';

  $function = 'lootz_' . $vendor . '_' . $hook;

  if (function_exists($function)) {
    return call_user_func_array($function, $args);
  }
}

/**
 * Load lootz cache
 */
function _lootz_cache_get($cid) {
  $cache = cache_get('lootz_' . $cid);
  if ($cache) {
    return $cache->data;
  }
  return array();
}

/**
 * Store lootz cache
 */
function _lootz_cache_set($cid, $value) {
  cache_set('lootz_' . $cid, $value, 'cache', CACHE_TEMPORARY);
}

/**
 * Return settings form
 */
function _lootz_filter_settings() {
  $form['lootz'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Lootz Settings'),
    '#collapsible' => TRUE,
  );
  $form['lootz']['lootz_item_database'] = array(
    '#type' => 'radios',
    '#title' => t('Item Database'),
    '#options' => _lootz_get_available_item_dbs(),
    '#default_value' => variable_get('lootz_item_database', 'wowdb'),
    '#description' => t('The item lookup database to use'),
  );
  return $form;
}

/**
 * Get all available item dbs (as configured in item_dbs directory)
 */
function _lootz_get_available_item_dbs() {
  // include all files
  $dir = drupal_get_path('module', 'lootz') . '/item_dbs';
  $available_item_dbs = file_scan_directory($dir, '\.inc$');
 
  $item_dbs = array();
  foreach (array_keys($available_item_dbs) as $file) {
    if (preg_match('#/([a-z]*)\.inc$#', $file, $m)) {
      // this is the name, eg wowhead, wowdb
      $item_db = $m[1];

      include_once $file;

      $function = 'lootz_' . $item_db . '_item_db_info';
      if (function_exists($function)) {
        $details = call_user_func_array($function, array());
        $item_dbs = array_merge($item_dbs, $details);
      }
    }
  }

  return $item_dbs;
}
